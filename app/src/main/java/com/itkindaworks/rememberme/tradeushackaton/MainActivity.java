package com.itkindaworks.rememberme.tradeushackaton;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    private TextView mNameViev;
    private ImageView mPhotoViev;
private TextView mRatingViev;
private TextView mAmountViev;
    private Button nextBttn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        User user = new User("Volodya","pass","vova",0d,0d);
        Stack<TradeHistory> mTrade = new Stack<>();
        mTrade.push(new TradeHistory(new Date(1998,12,54),5000d));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNameViev = (TextView)findViewById(R.id.name);
        mNameViev.setText(user.getName());

        mAmountViev = (TextView) findViewById(R.id.amount);

        mAmountViev.setText(String.valueOf(user.getBalance()));

        mRatingViev = (TextView) findViewById(R.id.rating);
        mRatingViev.setText(String.valueOf(user.getRating()));

        nextBttn = (Button)findViewById(R.id.trades);
        nextBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Trades.class);
                startActivity(intent);
            }
        });

    }
}
