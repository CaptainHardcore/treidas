package com.itkindaworks.rememberme.tradeushackaton;

/**
 * Created by remember me on 21.04.2017.
 */

public class User {
    public User(String name, String pass, String login,  double rating, double balance) {
        this.name = name;
        this.pass = pass;
        this.login = login;
       // mTradeHistories = tradeHistories;
        this.rating = rating;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

  //  public TradeHistory getTradeHistories() {
      //  TradeHistory deal = mTradeHistories.pop();
       // return deal;
   // }

  //  public void setTradeHistories(TradeHistory tradeHistories) {
      //  mTradeHistories.push(tradeHistories);
   // }

    private String name;
    private String pass;
    private String login;
  //  private Stack<TradeHistory> mTradeHistories;
    private double rating;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    private double balance;

  /** public double getRating(Stack<TradeHistory> mTradeHistories) {
       int allTrades = 0;
        int vonTrades = 0;

        while (!mTradeHistories.isEmpty()){
            if (mTradeHistories.pop().isVon()){
                allTrades ++;
                vonTrades ++;
            }
            else
            {
                allTrades++;
            }
        }
        double rating = (double) vonTrades/allTrades;
        return rating;
    }*/
  public double getRating(){
        return rating;
    }


}
