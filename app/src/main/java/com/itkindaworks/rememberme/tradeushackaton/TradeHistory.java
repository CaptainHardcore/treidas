package com.itkindaworks.rememberme.tradeushackaton;

import java.util.Date;

/**
 * Created by remember me on 21.04.2017.
 */

public class TradeHistory {
    public TradeHistory(Date date, double amount) {
        mDate = date;
        this.amount = amount;

    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isVon() {
       return isVon;
    }

    public void setVon(boolean von) {
        isVon = von;
    }

    private Date mDate;
    private double amount;
    private boolean isVon;

}
